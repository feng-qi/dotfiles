Installation:

```sh
git clone https://github.com/feng-qi/dotfiles.git ~
```


Create symlinks:

```sh
ln -s ~/dotfiles/spacemacs.d ~/.spacemacs.d
ln -s ~/dotfiles/vimrc ~/.vimrc
ln -s ~/dotfiles/UltiSnips/ ~/.vim/UltiSnips

ln -s ~/dotfiles/agignore ~/.agignore
ln -s ~/dotfiles/zshrc ~/.zshrc
```
